import os
import re
import sys
import glob
import zlib
import hashlib
import argparse

argparser = argparse.ArgumentParser(description="PreGit")
argsubparsers = argparser.add_subparsers(
    title="Pregit commands", dest="command")
argsubparsers.required = True

# for git init
argsp = argsubparsers.add_parser("init", help="Initialize empty repository")
argsp.add_argument("pth",
                   metavar="directory",
                   nargs="?",
                   default=".",
                   help="Path to the new repository.")

argsubparsers.add_parser("commit", help="Commit all files")

argsubparsers.add_parser("commit", help="Commit all files")

argsp = argsubparsers.add_parser(
    "checkout", help="Checkout to the commit with id=cid.")
argsp.add_argument("cid",
                   nargs="?",
                   help="Commit id to checkout.")


class Repo(object):
    def __init__(self, pth):
        self.worktree = pth
        self.gitdir = os.path.join(pth, ".pregit")


def get_repo_path(repo, *pth):
    return os.path.join(repo.gitdir, *pth)


def get_repo_file(repo, *pth, mkdir=False):
    if create_repo_dir(repo, *pth[:-1], mkdir=mkdir):
        return get_repo_path(repo, *pth)


def create_repo_dir(repo, *pth, mkdir=False):
    pth = get_repo_path(repo, *pth)

    if os.path.exists(pth):
        if (os.path.isdir(pth)):
            return pth
        else:
            raise Exception("Not a directory %s" % pth)

    if mkdir:
        os.makedirs(pth)
        return pth
    else:
        return None


def init_repo(pth):
    repo = Repo(pth)

    # Check if the input path exists or an empty dir
    if os.path.exists(repo.worktree):
        if not os.path.isdir(repo.worktree):
            raise Exception("%s is not a directory" % pth)
        if os.listdir(repo.worktree):
            raise Exception("%s is not empty" % pth)
    else:
        os.makedirs(repo.worktree)

    assert(create_repo_dir(repo, "branches", mkdir=True))
    assert(create_repo_dir(repo, "objects", mkdir=True))
    assert(create_repo_dir(repo, "refs", "tags", mkdir=True))
    assert(create_repo_dir(repo, "refs", "heads", mkdir=True))

    # .git/HEAD
    with open(get_repo_file(repo, "HEAD"), "w") as f:
        f.write("ref: refs/heads/master\n")

    return repo


def cmd_init(args):
    init_repo(args.pth)


class GitBlob(object):
    def __init__(self, repo, data):
        self.repo = repo
        self.blobdata = data

    def serialize(self):
        return self.blobdata

    def deserialize(self, data):
        self.blobdata = data


def object_read(repo, sha):
    """Read object object_id from Git repository repo.  Return a
    GitObject whose exact type depends on the object."""

    path = get_repo_file(repo, "objects", sha[0:2], sha[2:])

    with open(path, "rb") as f:
        raw = zlib.decompress(f.read())
        # Read object type
        x = raw.find(b' ')
        # Read and validate object size
        y = raw.find(b'\x00', x)
        size = int(raw[x:y].decode("ascii"))
        if size != len(raw)-y-1:
            raise Exception("Malformed object {0}: bad length".format(sha))

        return raw[y+1:]


def object_write(obj, actually_write=True):
    data = obj.serialize()
    # Add header
    result = b' ' + str(len(data)).encode() + b'\x00' + data
    # Compute hash
    sha = hashlib.sha1(result).hexdigest()

    if actually_write:
        # Get path
        path = get_repo_file(obj.repo, "objects",
                             sha[0:2], sha[2:], mkdir=actually_write)

        with open(path, 'wb') as f:
            # Compress and write
            f.write(zlib.compress(result))

    return sha


def get_repo_root():
    return glob.glob('**/.pregit', recursive=True)[0].split("/")[:-1][0]


def cmd_add():
    root = get_repo_root()
    repo = Repo(root)
    return [pregit_add(repo, fname) for fname in os.listdir(root) if not fname.startswith(".")]


def pregit_add(repo, fname):
    with open(f"{repo.worktree}/{fname}", "rb") as fd:
        obj = GitBlob(repo, fd.read())
        sha = object_write(obj)
        return sha, fname


def cmd_commit(hashed_list):
    root = get_repo_root()
    repo = Repo(root)
    pregit_commit(repo, hashed_list)


def pregit_commit(repo, hashed_list):
    commit_id = hashlib.sha1(
        ";".join((sha[0] for sha in hashed_list)).encode()).hexdigest()
    with open(f"{repo.gitdir}/HISTORY", "a+") as h:
        h.seek(0)
        if commit_id not in (line.split(":")[0] for line in h.readlines()):
            h.write(f"{commit_id}:")
            h.write(str(hashed_list) + '\n')


def cmd_checkout(args):
    root = get_repo_root()
    repo = Repo(root)
    pregit_checkout(repo, args.cid)


def pregit_checkout(repo, commit_id):
    commit_dct = dict()
    with open(f"{repo.gitdir}/HISTORY", "r") as h:
        contents = [line.split(":")[1] for line in h.readlines()
                    if line.split(":")[0] == commit_id][-1]
        proc_contents = list(filter(lambda x: x != ",", re.findall(
            '\[[^\]]*\]|\([^\)]*\)|\"[^\"]*\"|\S+', contents[1:-2])))
        for content in proc_contents:
            sha, filename = content[1:-1].split(",")
            # filename = ''.join(e for e in filename if e == "." or e.isalnum())
            filename = filename[2:-1]
            sha = ''.join(e for e in sha if e.isalnum())
            raw = object_read(repo, sha)
            commit_dct[filename] = raw

    for fname in os.listdir(repo.worktree):
        if not fname.startswith("."):
            os.remove(f"{repo.worktree}/{fname}")
    for fname, data in commit_dct.items():
        with open(f"{repo.worktree}/{fname}", "w+") as f:
            f.write(data.decode("utf-8"))


if __name__ == "__main__":
    args = argparser.parse_args(sys.argv[1:])
    if args.command == "init":
        cmd_init(args)
    elif args.command == "commit":
        hashed_list = cmd_add()
        cmd_commit(hashed_list)
    elif args.command == "checkout":
        hashed_list = cmd_checkout(args)
