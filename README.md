# PreGit prototype #

## PreGit init

### Run

```sh
python3 ./pregit.py init *your-path*
```

### Result
*your-path* will be populated with the `.pregit` file which will have the following structure:
```
.pregit
├── branches
├── objects
├── refs
|   ├── heads
|   └── tags 
└── HEAD.txt
```

## PreGit commit

### Run

```sh
python3 ./pregit.py commit
```

### Result
New commit will be added to .pregit/HISTORY:

File contents
```
commit id:[("sha256", "filename"), ("sha256", "filename1")]
```

## PreGit checkout

### Run

Commit id can be found in .pregit/HISTORY file

```sh
python3 ./pregit.py checkout *your-commit-id*
```

### Result
All files will be updated to match the commit state.
